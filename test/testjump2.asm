# test case 2

#[0, 0, 0, 0, 0]
main:
    li      a0, 1  # -> [1, 0, 0, 0, 0]
    li      a2, 3  # -> [1, 1, 1, 0, 0]
    li      a4, 4  # -> [1, 1, 1, 0, 1]
    jal     func   # -> (makes backup copy of the state: [1, 1, 1, 0, 1])  -> [1, 1, 1, 0, 0]
                   # -> (returns from func -> [1, 1, 1, 0, 0])
    
    add a5, a0, a1
    add a6, a2, a3
    add a7, a4, a4

    li a7, 10
    ecall

# [a0: 1, a1: 1, a2: 1, a3: 0, a4: 0]  -> since a3 is not set, by order it assumes a4 isn't set either
# -> a2 is still set because a2-a9 can be used for arguments
func:
    li      a1, 2        # -> [1, 1, 0, 0, 0]
    add     a3, a0, a1   # -> [1, 1, 1, 1, 0]  
    add     a2, a4, a1   # a4 should be uninitialized even though it was init in main
    jr      ra           # -> (a3 and a4 isn't used for returning value [1, 1, 1, 0, 0])
