# Hello World

# Welcome to the RISC-V Assembler and Workable, Rewritable System.
# We call it RAWRS. It's a dinosaur thing.

# It lets you play around with RISC-V assembly language and hopefully learn a
# little bit about computers along the way.

# Press the "Run" button at the top right to see what this program does and
# then return to look at the code!

# Here's some code (which historically we refer to as 'text', neat!)
.text

  # Say hello! using an environment call
  li    a7, 4
  la    a0, str_hello
  ecall

  la    a0, str_2
  ecall
  addi a1, zero, 5

  la    a0, str_3
  ecall
  addi a3, a1, 6

  la    a0, str_4
  ecall
  addi a5, a6, 9

  la    a0, str_1
  ecall

  # Power off the machine
  li    a7, 10
  ecall


# And here is all of the data (the... not code parts)
.data

str_hello:    .string   "Hello!"
str_1: .string "li:"
str_2: .string "addi a1, zero:"
str_3: .string "addi a3, a1:"
str_4: .string "addi a5, a6:"



