# test case 1

# [a0: 1, a1: 0, a2: 0, s0: 0, s1: 0, t0: 0, t1: 0]

main:
    move    s0, zero   # -> [0, 0, 0, 1, 0, 0, 0]
    move    t0, zero   # -> [0, 0, 0, 1, 0, 1, 0]

    li      a0, 1      # -> [1, 0, 0, 1, 0, 1, 0]
    jal     func       # -> (makes backup copy of state:[1, 0, 0, 1, 0, 1, 0]) -> [1, 0, 0, 0, 0, 0, 0]

    # Power off the machine
    li    a7, 10
    ecall            
           
func:
    add     a1, a0, a0 # -> [1, 1, 0, 0, 0, 0, 0]
    jr      ra         # -> (restores state of s-registers) -> [1, 1, 0, 1, 0, 0, 0]

# Power off the machine
li    a7, 10
ecall
