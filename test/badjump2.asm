# Hello World

# Welcome to the RISC-V Assembler and Workable, Rewritable System.
# We call it RAWRS. It's a dinosaur thing.

# It lets you play around with RISC-V assembly language and hopefully learn a
# little bit about computers along the way.

# Press the "Run" button at the top right to see what this program does and
# then return to look at the code!

# Here's some code (which historically we refer to as 'text', neat!)
.text

  # no jal before jr ra
  main:
    li a0, 3
    jr ra

  # Power off the machine
  li    a7, 10
  ecall


# And here is all of the data (the... not code parts)
.data
