../utils/bin/riscv64-unknown-elf-as test/hello.asm -o test/test.o
../utils/bin/riscv64-unknown-elf-ld test/test.o -o test/test.elf -T linker.ld

./temu -m 32 config.json

or

gdb temu 
r "-m" "32" "config.json"