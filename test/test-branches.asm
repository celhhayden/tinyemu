# Hello World

# Welcome to the RISC-V Assembler and Workable, Rewritable System.
# We call it RAWRS. It's a dinosaur thing.

# It lets you play around with RISC-V assembly language and hopefully learn a
# little bit about computers along the way.

# Press the "Run" button at the top right to see what this program does and
# then return to look at the code!

# Here's some code (which historically we refer to as 'text', neat!)
.text
  
  # set only a few regs with values so their flags are set for testing
  li a0, 5
  li a1, 8

  # check if uninit reg is 0
  _first:
    beqz a2, _second

  # check init reg != 0
  _second:
    bnez a0, _third

  # check uninit reg != 0
  _third:
    beqz a3, _fourth

  # check init reg > 0
  _fourth:
    bgtz a1, _fifth

  # check uninit reg <= 0
  _fifth:
    blez a4, _end

  # check nop
  _end:
    nop

  # Power off the machine
  li    a7, 10
  ecall

# And here is all of the data (the... not code parts)
.data
