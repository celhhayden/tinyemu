# test case 3

# [0, 0, 0, 0, 0, 0]
main:
    move  s0, zero  # -> [0, 0, 0, 1, 0]
    move  s2, zero  
    move  a0, zero  # -> [1, 0, 0, 1, 0]

    jal     func    # -> (makes backup copy of the state: [1, 0, 0, 1, 0])   -> [1, 0, 0, 0, 0]
                    # -> returns from func [1, 1, 0, 1, 0]

    li a7, 10
    ecall

# [a0: 1,  a1: 0,  a2: 0, s0: 0,  s1:0]
func:
    addi sp, sp, -16
    sw   s0, 8(sp)    # -> [1, 0, 0, 0, 0] ->(correct because it's backing up registers)
    sw   s1, 4(sp)    # -> [1, 0, 0, 0, 0] ->(correct because it's backing up registers)

    li   s1, 1        # -> [1, 0, 0, 0, 1]
    li   s2, 8
    add  s0, s1, a0   # -> [1, 0, 0, 1, 1]
    add  a1, s0, a0   # -> [1, 1, 0, 1, 1]


    lw   s0, 8(sp)    # -> [1, 1, 0, 1, 1]
    lw   s1, 4(sp)    # -> [1, 1, 0, 1, 1]
    addi sp, sp, 16

    jr   ra           # -> (restores state of s registers, a1 can be used for return [1, 1, 0, 1, 0])
